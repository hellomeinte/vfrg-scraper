const puppeteer = require("puppeteer");
const https = require("https");
const pdftk = require("node-pdftk");
const debug = require("debug")("vfrg-scrape");

const ROOTURL = "https://vfrg.casa.gov.au/";

const grabMenuLinks = async (page, menuSelector) => {
  await page.waitForSelector(menuSelector);
  return await page.evaluate(menuSelector => {
    const links = [];
    const anchors = document.querySelectorAll(`${menuSelector} a`);
    anchors.forEach(anchor => {
      links.push({
        title: anchor.innerHTML,
        href: anchor.href
      });
    });
    return links;
  }, menuSelector);
};

const checkIf200 = async url => {
  return new Promise(resolve => {
    https.get(url, response => {
      resolve(response.statusCode >= 200 && response.statusCode < 300);
    });
  });
};

const generateBookmark = (bookmarkTitle, level = 1, dumpData = "") =>
  `BookmarkBegin
BookmarkTitle: ${bookmarkTitle}
BookmarkLevel: ${level}
BookmarkPageNumber: 1
${dumpData}`;

(async () => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.setViewport({ width: 1000, height: 926 });
  debug("Initializing on: " + ROOTURL);
  await page.goto(ROOTURL);

  //first level
  const menuLinks = await grabMenuLinks(page, "ul.menu");
  const files = [];
  //skip last menu link item 'resources', as it is not useful in pdf context
  for (var j = 0; j < menuLinks.length - 1; j++) {
    const menuLink = menuLinks[j].href;
    const firstLinkName = menuLinks[j].title;
    debug("visiting " + menuLink);
    await page.goto(menuLink);
    const subLinks = await grabMenuLinks(page, "ul.menu");

    //third level
    const subLink = subLinks[0].href;
    debug("visiting subLink " + subLink);
    await page.goto(subLink);

    const finalLinks = await grabMenuLinks(page, "div.accordion");

    let categoryTitle = "";
    for (var i = 0; i < finalLinks.length; i++) {
      const fLink = finalLinks[i];
      if (!fLink) continue;
      if (!fLink.href) continue;
      const stdStr = `Progress: overall: ${j}/${menuLinks.length -
        1} current:${i}/${finalLinks.length - 1}`;
      debug(stdStr);
      process.stdout.write(`${stdStr}\r`);
      const is200 = await checkIf200(fLink.href);
      debug(`${fLink.href} is200: ${is200}`);
      //get rid of redirects (puppeteer for now can't gracefully do this)

      if (!is200) {
        categoryTitle = fLink.title.split("<")[0];
        debug(`New Category title: ${categoryTitle}`);
        continue;
      }
      const fileName = `./tmp/${files.length}.pdf`;
      files.push(fileName);
      debug("visiting subpage " + fLink.href);
      await page.goto(fLink.href);
      //print-disclaimer
      await page.evaluate(() => {
        const disclaimer = document.querySelector(".print-disclaimer");
        disclaimer.parentNode.removeChild(disclaimer);
      });
      await page.pdf({ path: fileName, format: "A4" });
      const subTitle = fLink.title.split("<")[0];
      const tTitle = categoryTitle ? `${categoryTitle}: ${subTitle}` : subTitle;
      debug("opening " + fileName + "title= " + tTitle);
      const infoString = await pdftk
        .input(fileName)
        .dumpData()
        .output()
        .then(buffer =>
          generateBookmark(
            `${firstLinkName}: ${tTitle}`,
            1,
            buffer.toString("utf8")
          )
        )
        .catch(e => {
          debug("Error opening file  to dumpdata" + fileName);
          debug(e);
        });

      await pdftk
        .input(fileName)
        .updateInfo(pdftk.PdfTk.infoStringToObject(infoString))
        .output(fileName)
        .catch(e => {
          debug("Error opening file to update" + fileName);
          debug(infoString);
          debug(e);
        });
    }
  }
  await browser.close();

  await pdftk
    .input(files)
    .cat()
    .output("./tmp/concat.pdf")
    .catch(e => {
      debug("Error concatenating files" + files);
      debug(e);
    });

  const serialisedInfo = await pdftk
    .input("./tmp/concat.pdf")
    .dumpData()
    .output()
    .then(buffer => pdftk.PdfTk.infoStringToObject(buffer.toString("utf8")))
    .then(data => {
      //first filter out invalid bookmarks
      data.Bookmark = data.Bookmark.filter(bookmark => {
        return bookmark.Title.length > 1;
      });
      return data;
    })
    .then(data => {
      const modifiedBookmarks = [];
      let currentMain = "";
      let currentTopic = "";
      data.Bookmark.forEach(bookmark => {
        const bTitle = bookmark.Title;
        const splitTitle = bTitle.split(":");
        const main = splitTitle[0] || bTitle.trim();
        const topic = splitTitle[1] || main || bTitle.trim();
        const title = splitTitle[2] || topic || bTitle.trim();

        bookmark.Title = title;
        if (main !== currentMain) {
          const mainHeader = { ...bookmark };
          mainHeader.Title = main;
          modifiedBookmarks.push(mainHeader);
          currentMain = main;
        }

        if (topic !== currentTopic) {
          const topicHeader = { ...bookmark };
          const level = parseInt(topicHeader.Level, 10) + 1;
          topicHeader.Title = topic;
          topicHeader.Level = level.toString();
          modifiedBookmarks.push(topicHeader);
          currentTopic = topic;
        }

        const level = parseInt(bookmark.Level, 10) + 2;
        bookmark.Level = level.toString();
        modifiedBookmarks.push(bookmark);
      });
      data.Bookmark = modifiedBookmarks;
      return data;
    });

  await pdftk
    .input("./tmp/concat.pdf")
    .updateInfo(serialisedInfo)
    .output("vfrg.pdf");

  process.stdout.write(`Done, vfrg.pdf\n`);
})();
