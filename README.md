## Synopsis

This is a scraper for the popular VFR guide, written by CASA. It runs, scrapes, bookmarks and then collates into a single PDF file for offline use. It uses my fork of node-pdftk, which I will revert once that PR has been merged.

## Motivation

A lot of people(including myself) require access to an offline copy of the VFR guide from time to time, or simply to put on an ereader. There used to be a PDF available, but this is no longer the case. This scraper simply goes through the online page and produces a bookmarked PDF file for easy access.

## Installation

Make sure you have node, npm and pdftk installed. The latter one can be downloaded from https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/. There is an issue though where the latest version for the mac is not mentioned on that site. This is important because the version that is accesible on there no longer works for the latest osx version. Hopefully this will be resolved, but until then, here's a post with the solution; https://stackoverflow.com/questions/39750883/pdftk-hanging-on-macos-sierra

After all that is done

`npm install`
`npm run production`

## Tests

No tests are available for now.
